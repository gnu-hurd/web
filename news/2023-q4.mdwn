[[!meta copyright="Copyright © 2013 Free Software Foundation, Inc."]]

[[!meta license="""[[!toggle id="license" text="GFDL 1.2+"]][[!toggleable
id="license" text="Permission is granted to copy, distribute and/or modify this
document under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no Invariant
Sections, no Front-Cover Texts, and no Back-Cover Texts.  A copy of the license
is included in the section entitled [[GNU Free Documentation
License|/fdl]]."]]"""]]

[[!meta date="2024-01-05 22:22 UTC"]]

Hello!  Welcome to a new qoth.  This qoth covers new and interesting GNU/Hurd
developments in Q4 of 2023!
[[!if test="included()" then="""[[!toggle id=full_news
text="Details."]][[!toggleable id=full_news text="[[!paste id=full_news]]"]]"""
else="
[[!paste id=full_news]]"]]

[[!cut id="full_news" text="""

Samuel Thibault fixed gcc's Hurd's default pie and [[added static pie
support|https://lists.gnu.org/archive/html/bug-hurd/2023-10/msg00061.html]].
He also added a [[whatis
command|https://lists.gnu.org/archive/html/bug-hurd/2023-10/msg00007.html]]
to GNU Mach's kernel debugger, which can determine what an
address points to (a stack? a port? some kalloc? ...).  He also added
[[hurd-amd64 support to
GCC|https://lists.debian.org/debian-hurd/2023/11/msg00039.html]].

Samuel requested that the Hurd team set up a [[continuous
integration,|https://lists.gnu.org/archive/html/bug-hurd/2023-12/msg00007.html]]
so that when developers make code changes, they can be certain that
they did not break anything.  It turns out that the Hurd supports
several different environments: 32 bit, 64 bit, 32-on-64 bit, ACPI,
non-ACPI, SMP, non-SMP, Xen, etc.  Apparently Flavio has a [[personal
CI|https://github.com/flavioc/cross-hurd/actions/runs/7080757561]],
but it is set up in a Debian independent way.  If you are interested in
helping the Hurd project set up a CI, then please get in touch!

Luca Dariz worked on adding [[some simple GNU Mach user-space tests
|https://lists.gnu.org/archive/html/bug-hurd/2023-10/msg00031.html]].
With a working MiG, a GNU/Linux machine can run make check in the GNU
Mach source code, which will launch qemu to ensure that 32 bit (PAE
and non PAE), 32 on 64 bit, and full 64 bit GNU Mach works.  We
currently do this testing on GNU/Linux, because qemu does not run on
the Hurd.

Many people worked on the Hurd's new [[x86_64 bit
support|https://lists.gnu.org/archive/html/bug-hurd/2023-10/msg00036.html]].
A 64-bit debian buildd is set up, and we can bootstrap a chroot!  The
hurd-amd64 wanna-build infrastructure is also set up.  We are having
issues reliably building packages on a 64-bit Hurd, which lead Samuel
to uncover and fix [[a proc
leak|https://lists.gnu.org/archive/html/bug-hurd/2023-11/msg00035.html]].

Flavio Cruz [[improved GNU Mach's
IPC|https://lists.gnu.org/archive/html/bug-hurd/2023-11/msg00033.html]]
by reordering `mach_msg_type_t` fields to byte align `msgt_name` and
`msgt_size`.  He also created a patch series to [[avoid message
resizing for
x86_64|https://lists.gnu.org/archive/html/bug-hurd/2023-11/msg00028.html]].
He also [[removed untyped mach RPC
code|https://lists.gnu.org/archive/html/bug-hurd/2023-11/msg00026.html]].
GNU Mach uses typed IPC.  The Hurd could support both typed and
untyped, but it appears that the Hurd only uses typed RPC.  So it
seems best to remove any untyped RPC code.

Sergey Bugaev added [[GNU Mach entry re-coalescing
support|https://darnassus.sceen.net/~hurd-web/open_issues/gnumach_vm_map_entry_forward_merging/]].
Essentially, Mach was not always able to merge two vm entries that are
made next to each other, which was slowing down ext2, bash, etc.  Sergey
allowed GNU Mach to merge entries in the common cases, which greatly
helps ext2fs for instance.

Sergey is also working on [[porting the Ladybird web
browser|https://lists.gnu.org/archive/html/bug-hurd/2023-11/msg00013.html]]
to the Hurd.  The author of this post uses the [[netsurf web
browser|https://www.netsurf-browser.org/]] on the Hurd, which works on
simple websites like wikipedia, but it badly renders javascript heavy
websites, which makes many websites un-useable.  If Sergey is
successful in porting [[Ladybird|https://ladybird.dev/]], then Hurd
users could start using sites like Github!  It is worth noting that
someone should update the [[Firefox
port|https://lists.debian.org/debian-hurd/2014/09/msg00070.html]] as
well.

Sergey also started [[porting the Hurd to
AArch64!|https://lists.gnu.org/archive/html/bug-hurd/2023-12/msg00110.html]]
While a port to RISC-V might be more exciting, it is worth mentioning
that AArch64 is more established. What is interesting is that Sergey
is already able to build Hurd servers for AArch64! Normally, in order
to run the binaries, one would port GNU Mach to AArch64. Luckily for
us, he turned to GDB and directly ran a 'Hello World' Hurd AArch64
binary on Linux!  This helped him fix some bugs along the way.  We
still need to define the ABI and complete the GNU Mach port, but this
is exciting news!

Tobias Platen started [[porting GNU Mach to
Power9|https://lists.gnu.org/archive/html/bug-hurd/2023-10/msg00021.html]].

So if you want to test if your favorite packages work on the Hurd and
contribute towards making the full GNU system usable for a wider range
of people, please [[check the contributing page|contributing]].

---

The **GNU Hurd** is the GNU project's replacement for the Unix kernel.  It is a
collection of servers that run on the Mach microkernel to implement file
systems, network protocols, file access control, and other features that are
implemented by the Unix kernel or similar kernels (such as Linux).  [[More
detailed|hurd/documentation]].

**GNU Mach** is the microkernel upon which a GNU Hurd system is based.  It
provides an Inter Process Communication (IPC) mechanism that the Hurd uses to
define interfaces for implementing in a distributed multi-server fashion the
services a traditional operating system kernel provides.  [[More
detailed|microkernel/mach/gnumach]].

"""]]
